package com.myntra.test;

import com.myntra.airbus.consumer.AirbusConsumer;
import com.myntra.airbus.entry.EventEntry;
import com.myntra.airbus.entry.EventListenerEntity;
import com.myntra.airbus.exception.ManagerException;
import com.myntra.airbus.shaded.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class AirbusConsumerTest {

    public static void main(String[] args) throws ManagerException, InterruptedException, ExecutionException {
        if (args.length < 3) {
            System.out.println("Please provide command line arguments 1. producer app name 2. producer event name 3. consumer app name 4. service url");
            System.exit(-1);
        }
        System.out.println("Starting script for consumer ");
        EventEntry entryOne = new EventEntry();
        entryOne.setAppName(args[0]);
        entryOne.setEventName(args[1]);
        EventListenerEntity listenerOne = new EventListenerEntity(entryOne, 1, new MyListenerOne());

        Set<EventListenerEntity> listenerSet = new HashSet<EventListenerEntity>();
        listenerSet.add(listenerOne);

        Map<String, Object> map = new HashMap<String, Object>();
        // map.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        map.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "50");

        AirbusConsumer airbusConsumer = new AirbusConsumer(listenerSet, args[3], map, args[2]);
        System.out.println("Consumer started " + airbusConsumer);
    }
}
