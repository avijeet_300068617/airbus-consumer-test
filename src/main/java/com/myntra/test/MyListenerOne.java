package com.myntra.test;

import com.myntra.airbus.consumer.AirbusEventListener;
import com.myntra.airbus.exception.ManagerException;

public class MyListenerOne implements AirbusEventListener {

    public void onEvent(String key, String value) throws ManagerException {
        System.out.println("Received message " + value);
    }
}
